<?php
declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\AbortException;
use Nette\Application\UI\Presenter;


/**
 * Base presenter for all application presenters.
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{

    /**
     * Redraws snippets if request is Ajax, otherwise reloads page
     * @param array|null $snippets
     * @param null|string $destination
     * @throws AbortException
     */
    public function redirectAjax(array $snippets = null, string $destination = null): void
    {
        if ($this->isAjax()) {
            if ($snippets === null) {
                $this->redrawControl();
            } else {
                foreach ($snippets as $snippet) {
                    $this->redrawControl($snippet);
                }
            }
        } else {
            if ($destination === null) {
                $this->redirect('this');
            } else {
                $this->redirect('this', $destination);
            }
        }
    }
}
