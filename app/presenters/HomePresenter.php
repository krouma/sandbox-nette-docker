<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 19.11.16
 * Time: 22:04
 */

namespace App\Presenters;


/**
 * Class HomePresenter
 * @package App\Presenters
 * @author matyas
 */
class HomePresenter extends BasePresenter
{

    public function renderDefault(): void
    {
    }
}
