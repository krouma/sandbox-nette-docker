<?php
declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use Nette\Database\Context;

/**
 * Description of BaseManager
 * Base class for other managers
 * @package App\Model
 * @author matyas
 */
abstract class BaseManager
{

    /** @var Context Instance of class to work with DB */
    protected $database;

    /**
     * @param Context $database automatically injected class to work with DB
     */
    public function __construct(Context $database)
    {
        $this->database = $database;
    }
}
