<?php
declare(strict_types=1);

namespace App;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Default Router Factory
 * Controls routing whole app
 * @package App
 */
class RouterFactory
{
    /**
     * Creates router for app
     * @return RouteList
     */
    public static function createRouter(): RouteList
    {
        $router = new RouteList();
        $router[] = new Route('', 'Core:Home:default');
        return $router;
    }
}
